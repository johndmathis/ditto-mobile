# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security


## [v0.2.1] - 2020-01-28

### Added

- Simple typing indicators
- Linting with StandardJS rules

### Changed

- Auto-focus on first login input
- Smaller text on bottom navigation tabs

### Fixed

- Swipe to dismiss keyboard
- Android can swipe between Messages and Groups
- Show notifications on Android when the app is in the background
- Open the Chat Room when the app is launched with a notification
- Update the Chat Room when the roomId changes


## [v0.2.0] - 2020-01-21

### Added

- Ability to Create a New Room & Search for Users
- Notifications for Android
- Improved Timeline Rendering (support for m.notice)
- Settings Screen where you can change your avatar and display name and enable/disable notifications
- Privacy Policy

### Changed

- Color scheme is darker / night mode
- Improved Direct Message vs Group Detection
- Changed Gifted Chat for Custom Message Bubbles

### Fixed

- Various Visual Bugs


## [v0.1.7] - 2019-12-11

### Added

- Changelog.md
- Contributing.md
- Assets for Android release
- Modals for settings / create a room

### Changed

- Names in chats have higher contrast

### Fixed

- Blank screen on Android restart
- Message composer hidden behind Android keyboard
- Jumping to bottom of screen on load previous message


## [v0.1.6] - 2019-12-05

### Added

- Scripts for development
- Display avatars and display names
- Add Android launcher
- Receive images

### Changed

- Refined design


## [v0.0.5] - 2019-11-24

First release of the app

### Added

- Init React Native project
- Login with a Matrix account
- Send and receive messages
- App logo
- Set up CodePush


[Unreleased]: https://gitlab.com/ditto-chat/ditto-mobile/compare/0.2.1...dev
[0.2.1]: https://gitlab.com/ditto-chat/ditto-mobile/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/ditto-chat/ditto-mobile/compare/0.1.7...0.2.0
[0.1.7]: https://gitlab.com/ditto-chat/ditto-mobile/compare/0.1.6...0.1.7
[0.1.6]: https://gitlab.com/ditto-chat/ditto-mobile/compare/0.0.5...0.1.6
[0.0.5]: https://gitlab.com/ditto-chat/ditto-mobile/-/tags/0.0.5
