import 'react-native-gesture-handler'

import React from 'react'
import { Provider } from 'react-redux'

import AppContainer from './src/AppContainer'
import store from './src/store'

// import * as Sentry from '@sentry/react-native';
// Sentry.init({
//   dsn: 'https://42044a4916614554874cdaf96ea70e59@sentry.io/1529001',
// });

const debug = require('debug')
debug.enable('ditto:*')

console.disableYellowBox = true

const App = () => (
  <Provider store={store}>
    <AppContainer />
  </Provider>
)

export default App
