import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'

import RootReducer from './reducers'

type IUser = {
  userId: string,
  accessToken: string,
  displayName: string
};

export type IMatrixState = {
  client: any, // the matrix client
  homeserver: string,
  currentMessages: any[],
  currentRoom: string,
  user: IUser,
  syncError: any,
  isSyncing: boolean
};

export type IOverallAppState = {
  newChatModalVisible: boolean
};

export type IAppState = {
  matrix: IMatrixState,
  app: IOverallAppState
};

const store = createStore(RootReducer, applyMiddleware(thunk))
export default store
