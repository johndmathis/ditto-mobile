import React from 'react'

import ChevronLeft from '../assets/icons/icon-chevron-left.svg'
import { COLORS } from '../constants'

const size = 30

const BackIcon = () => (
  <ChevronLeft fill={COLORS.gray.one} width={size} height={size} />
)
export default BackIcon
