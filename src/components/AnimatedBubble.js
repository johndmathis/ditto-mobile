// import React from 'react';
// import {connect} from 'react-redux';
// import {Animated} from 'react-native';
// import styled from 'styled-components/native';
// import LinearGradient from 'react-native-linear-gradient';
// import {userSelector} from '../../redux/selectors';

// class AnimatedBubble extends React.Component {
//   scaleValue = new Animated.Value(0);
//   transformValue = new Animated.Value(0);

//   componentDidMount = () => {
//     Animated.parallel([
//       Animated.spring(this.transformValue, {
//         toValue: 1,
//         friction: 7,
//       }),
//       Animated.spring(this.scaleValue, {
//         toValue: 1,
//         friction: 7,
//       }),
//     ]).start();
//   };

//   render() {
//     // const {currentMessage, userId} = this.props;

//     let bubbleMove = this.transformValue.interpolate({
//       inputRange: [0, 1],
//       outputRange: [30, 0],
//     });
//     const bubbleScale = this.scaleValue.interpolate({
//       inputRange: [0, 1],
//       outputRange: [0.5, 1],
//     });

//     let translateStyle = {
//       transform: [{translateY: bubbleMove}, {scale: bubbleScale}],
//     };
//     return (
//       <Bubble
//         sentByMe={currentMessage.user._id === userId}
//         animatedStyle={translateStyle}>
//         {currentMessage.text}
//       </Bubble>
//     );
//   }
// }
// const mapStateToProps = state => ({
//   userId: userSelector(state).userId,
// });
// export default connect(
//   mapStateToProps,
//   null,
// )(AnimatedBubble);

// const DarkBackground = styled.View`
//   background-color: #3e424b;
//   /* background-color: transparent; */
// `;

// const MessageWrapper = styled(Animated.View)`
//   ${({sentByMe}) => (sentByMe ? 'align-self: flex-end;' : '')}
//   flex-direction: row;
//   align-items: flex-end;
//   margin: 3px 8px;
//   z-index: 1;
// `;

// const MessageAvatar = styled.View`
//   background-color: #c4c4c4;
//   border-radius: 60;
//   width: 40;
//   height: 40;
//   margin-right: 8px;
// `;

// const BubbleWrapper = styled(LinearGradient)`
//   padding: 10px 15px;
//   border-radius: 15;
//   max-width: 75%;
// `;

// const BubbleText = styled.Text`
//   color: white;
// `;

// const myBubbleColor = ['#FF8D3B', '#F11616'];
// const otherBubbleColor = ['#606672', '#606672'];
// const Bubble = props => (
//   <DarkBackground>
//     <MessageWrapper style={props.animatedStyle} {...props}>
//       {!props.sentByMe && <MessageAvatar />}
//       <BubbleWrapper
//         colors={props.sentByMe ? myBubbleColor : otherBubbleColor}
//         useAngle={true}
//         angle={2}
//         angleCenter={{x: 0.5, y: 0.5}}>
//         <BubbleText>{props.children}</BubbleText>
//       </BubbleWrapper>
//     </MessageWrapper>
//   </DarkBackground>
// );
