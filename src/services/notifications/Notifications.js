import AsyncStorage from '@react-native-community/async-storage'
import { firebase } from '@react-native-firebase/messaging'
import { AppState, NativeEventEmitter, Platform } from 'react-native'

import {
  clientSelector,
  currentRoomSelector,
  pushNotificationsSelector
} from '../../selectors'
import store from '../../store'
import { showError } from '../../utilities/Misc'
import { updatePushNotifications } from '../app/actions'
import { getEventDetails, setPusherWithToken } from '../matrix/api'
import NavigationService from '../navigation/NavigationService'
import MessagingNotificationAndroid from './MessagingNotificationAndroid'

const debug = require('debug')('ditto:utilities:Notifications')

const ANDROID_NOTIFICATION_CHANNEL_MAIN = 'com.elequin.ditto-main'
const ANDROID_NOTIFICATION_PRIORITY_HIGH = 2
const ANDROID_NOTIFICATION_PRIORITY_LOW = 0

export const startupNotifications = async (enable = false) => {
  debug('startupNotifications')

  try {
    const enablePushNotifications = await AsyncStorage.getItem(
      'pushNotifications.enable'
    )
    debug('enablePushNotifications: ', enablePushNotifications)
    const pushNotifications = Object.assign(
      {},
      pushNotificationsSelector(store.getState())
    )
    if (
      (enablePushNotifications === null && pushNotifications.enable === true) ||
      (enablePushNotifications !== null &&
        enablePushNotifications === 'true') ||
      enable
    ) {
      pushNotifications.enable = true
      AsyncStorage.setItem('pushNotifications.enable', 'true')

      if (Platform.OS === 'android') {
        MessagingNotificationAndroid.createNotificationChannel({
          id: ANDROID_NOTIFICATION_CHANNEL_MAIN,
          name: 'Ditto Main Channel',
          description: 'Main Ditto Messages'
        })

        const eventEmitter = new NativeEventEmitter(
          MessagingNotificationAndroid
        )
        // Listen to opened notifications
        eventEmitter.addListener(
          'MessagingNotificationAndroid_opened',
          event => {
            debug('Notification opened event received:', event)
            if (event != null && event.roomId != null) {
              NavigationService.navigate('Chat', { roomId: event.roomId })
            }
          }
        )
        // Listen to replies in notifications
        eventEmitter.addListener(
          'MessagingNotificationAndroid_reply',
          event => {
            debug('Reply event received:', event)
            if (event != null) {
              replyInNotification(event.roomId, event.reply)
            }
          }
        )

        // Listen to FCM push notifications when app is opened
        pushNotifications.fcmListener = firebase
          .messaging()
          .onMessage(async remoteMessage => {
            debug('Received FCM Message while in Foreground:', remoteMessage)

            const data = remoteMessage.data
            if (data.event_id != null && data.room_id != null) {
              displayEventNotification(data.event_id, data.room_id)
            } else {
              debug(`Problem with eventId (${data.event_id}) or roomId(${data.room_id})`)
            }
          })

        if (pushNotifications.pushkey == null) {
          pushNotifications.pushkey = await firebase.messaging().getToken()
          debug(
            'Registered successfully with FCM token: ',
            pushNotifications.pushkey
          )
        } else {
          debug('Already got an FCM token: ', pushNotifications.pushkey)
        }

        await setPusherWithToken(pushNotifications.pushkey)

        store.dispatch(updatePushNotifications(pushNotifications))
      }
    } else {
      debug('Notifications are disabled')
    }
  } catch (e) {
    showError('Notifications not enabled', e.message)
  }
}

export const shutdownNotifications = async (disable = false) => {
  debug('shutdownNotifications')

  try {
    const pushNotifications = Object.assign(
      {},
      pushNotificationsSelector(store.getState())
    )
    if (disable) {
      pushNotifications.enable = false
      await AsyncStorage.setItem('pushNotifications.enable', 'false')
    }

    if (Platform.OS === 'android') {
      // Invalidate token
      await firebase.messaging().deleteToken()
      pushNotifications.pushkey = null

      // Remove MessagingNotificationAndroid listeners
      const eventEmitter = new NativeEventEmitter(MessagingNotificationAndroid)
      eventEmitter.removeAllListeners('MessagingNotificationAndroid_opened')
      eventEmitter.removeAllListeners('MessagingNotificationAndroid_reply')

      // Remove FCM listener
      pushNotifications.fcmListener()
      pushNotifications.fcmListener = null

      store.dispatch(updatePushNotifications(pushNotifications))
    }
  } catch (e) {
    debug('Notifications not disabled: ', e.message)
  }
}

// **********************************************
// Helpers
// **********************************************

// Get the notification from which the app was opened if it was in the background or closed
export const getInitialNotification = async () => {
  if (Platform.OS === 'android') {
    const notification = await MessagingNotificationAndroid.getInitialNotification()
    return notification
  }
}

// Cancel all notifications associated to a room
export const cancelNotifications = async roomId => {
  try {
    if (roomId == null || roomId === '') {
      throw Error('No roomId set')
    }
    if (Platform.OS === 'android') {
      debug('Cancelling notifications for room ', roomId)
      await MessagingNotificationAndroid.cancelNotification(roomId)
    }
  } catch (e) {
    debug("Couldn't cancel notifications: ", e.message)
  }
}

// Reply to a notification without opening the app
const replyInNotification = async (roomId, reply) => {
  try {
    const matrixClient = clientSelector(store.getState())

    if (roomId == null) {
      debug('No roomId')
      throw Error("Couldn't get the reply")
    }
    if (reply == null || reply === '') {
      debug('No reply to send')
      throw Error("Couldn't get the reply")
    }

    const replyEvent = await matrixClient.sendTextMessage(roomId, reply)
    await displayEventNotification({
      event_id: replyEvent.event_id,
      room_id: roomId
    })
  } catch (e) {
    showError('Error sending reply', e.message)
  }
}

// Display a notification with the event data
export const displayEventNotification = async (eventId, roomId) => {
  try {
    debug('App state: ', AppState.currentState)
    const currentRoom = currentRoomSelector(store.getState())
    if (AppState.currentState === 'active' && roomId === currentRoom) {
      // Don't display notification if app is in foreground and room is opened
      throw Error(`Room ${roomId} is opened`)
    } else if (AppState.currentState !== 'active' && roomId === currentRoom) {
      // Remove the notification if the app is reopened on the room
      AppState.addEventListener('change', nextAppState => {
        if (nextAppState === 'active') {
          cancelNotifications(roomId)
        }
      })
    }

    const eventDetails = await getEventDetails(eventId, roomId)

    // If we already have notifications for this room, append the new message to the older ones
    const oldNotifications = await MessagingNotificationAndroid.getActiveNotifications()
    debug('Active notifications:', oldNotifications)
    const oldNotification = oldNotifications.find(
      notif => notif.room.id === roomId
    )

    const newNotification = Object.assign({}, oldNotification, {
      channelId: ANDROID_NOTIFICATION_CHANNEL_MAIN,
      priority: ANDROID_NOTIFICATION_PRIORITY_HIGH,
      room: eventDetails.room,
      me: eventDetails.me.id
    })

    if (oldNotification == null || Object.keys(oldNotification).length === 0) {
      Object.assign(newNotification, {
        users: [],
        messages: []
      })
    }

    // Add or update "me" in users
    const meIndex = newNotification.users.findIndex(
      user => user.id === eventDetails.me.id
    )
    if (meIndex === -1) {
      newNotification.users.push(eventDetails.me)
    } else {
      newNotification.users.splice(meIndex, 1, eventDetails.me)
    }
    if (eventDetails.sender.id === eventDetails.me.id) {
      // Don't make a sound for user's replies
      newNotification.priority = ANDROID_NOTIFICATION_PRIORITY_LOW
    } else {
      // Add or update sender in users
      const senderIndex = newNotification.users.findIndex(
        user => user.id === eventDetails.sender.id
      )
      if (senderIndex === -1) {
        newNotification.users.push(eventDetails.sender)
      } else {
        newNotification.users.splice(senderIndex, 1, eventDetails.sender)
      }
    }

    const messageIndex = newNotification.messages.findIndex(
      message => message.id === eventDetails.message.id
    )
    if (messageIndex === -1) {
      newNotification.messages.push(eventDetails.message)
    } else {
      // Because we can update the body after a redaction
      newNotification.messages.splice(messageIndex, 1, eventDetails.message)
    }

    debug('Notification to display: ', newNotification)
    MessagingNotificationAndroid.notify(newNotification)
  } catch (e) {
    debug('Notification not displayed: ', e.message)
  }
}
