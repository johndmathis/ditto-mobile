import { IMatrixState } from '../../store'
// import {MatrixActionTypes} from '../actions/MatrixActions'

// const debug = require('debug')('ditto:redux:reducer:MatrixReducer')

export const MatrixActionTypes = {
  SET_MATRIX_CLIENT: 'SET_MATRIX_CLIENT',
  SET_AUTH_ERROR: 'SET_AUTH_ERROR',
  CLEAR_AUTH_ERROR: 'CLEAR_AUTH_ERROR',
  SET_SYNC_ERROR: 'SET_SYNC_ERROR',
  UPDATE_IS_SYNCING: 'UPDATE_IS_SYNCING',
  UPDATE_NEW_MSG: 'UPDATE_NEW_MSG',
  SET_USER_DATA: 'SET_USER_DATA',
  SET_ROOM_LIST: 'SET_ROOM_LIST',
  SET_CURRENT_ROOM: 'SET_CURRENT_ROOM',
  SET_CURRENT_MESSAGES: 'SET_CURRENT_MESSAGES',
  UPDATE_TYPING_USERS: 'UPDATE_TYPING_USERS',
  USER_LOGOUT: 'USER_LOGOUT'
}

const initialState: IMatrixState = {
  client: null,
  userData: null,
  authError: null,
  syncError: null,
  isSyncing: true,
  roomList: [],
  currentRoom: null,
  currentMessages: [],
  newMessage: false,
  typingUsers: []
}

const MatrixReducer = (state = initialState, action) => {
  // debug('Action: ', action)
  switch (action.type) {
    case MatrixActionTypes.SET_MATRIX_CLIENT:
      return { ...state, client: action.payload }
    case MatrixActionTypes.SET_AUTH_ERROR:
      return { ...state, authError: action.payload }
    case MatrixActionTypes.CLEAR_AUTH_ERROR:
      return { ...state, authError: null }
    case MatrixActionTypes.SET_SYNC_ERROR:
      return { ...state, syncError: action.payload }
    case MatrixActionTypes.UPDATE_IS_SYNCING:
      return { ...state, isSyncing: action.payload }
    case MatrixActionTypes.UPDATE_NEW_MSG:
      return { ...state, newMessage: action.payload }
    case MatrixActionTypes.SET_USER_DATA:
      return { ...state, userData: action.payload }
    case MatrixActionTypes.SET_ROOM_LIST:
      return { ...state, roomList: action.payload }
    case MatrixActionTypes.SET_CURRENT_ROOM:
      return { ...state, currentRoom: action.payload }
    case MatrixActionTypes.SET_CURRENT_MESSAGES:
      return { ...state, currentMessages: action.payload }
    case MatrixActionTypes.USER_LOGOUT:
      return initialState
    case MatrixActionTypes.UPDATE_TYPING_USERS:
      return { ...state, typingUsers: action.payload }
    default:
      return state
  }
}
export default MatrixReducer
