import '../../utilities/poly'

import _ from 'lodash'
import sdk from 'matrix-js-sdk'

import {
  clientSelector,
  currentMessagesSelector,
  currentRoomSelector,
  roomListSelector,
  typingUsersSelector,
  userDataSelector
} from '../../selectors'
import { clearCache, logCache } from '../../utilities/Caching'
import {
  retrieveHomeserverFromCache,
  retrieveUserDataFromCache,
  saveHomeserverToCache,
  saveUserDataToCache
} from './api'

const debug = require('debug')('ditto:redux:MatrixActions')

export const MatrixActionTypes = {
  SET_MATRIX_CLIENT: 'SET_MATRIX_CLIENT',
  SET_AUTH_ERROR: 'SET_AUTH_ERROR',
  CLEAR_AUTH_ERROR: 'CLEAR_AUTH_ERROR',
  SET_SYNC_ERROR: 'SET_SYNC_ERROR',
  UPDATE_IS_SYNCING: 'UPDATE_IS_SYNCING',
  UPDATE_NEW_MSG: 'UPDATE_NEW_MSG',
  SET_USER_DATA: 'SET_USER_DATA',
  SET_ROOM_LIST: 'SET_ROOM_LIST',
  SET_CURRENT_ROOM: 'SET_CURRENT_ROOM',
  SET_CURRENT_MESSAGES: 'SET_CURRENT_MESSAGES',
  USER_LOGOUT: 'USER_LOGOUT',
  UPDATE_TYPING_USERS: 'UPDATE_TYPING_USERS'
}

const setMatrixClient = client => ({
  type: MatrixActionTypes.SET_MATRIX_CLIENT,
  payload: client
})

const setAuthError = error => ({
  type: MatrixActionTypes.SET_AUTH_ERROR,
  payload: error
})

export const clearAuthError = () => ({
  type: MatrixActionTypes.CLEAR_AUTH_ERROR
})

export const setSyncError = error => ({
  type: MatrixActionTypes.SET_SYNC_ERROR,
  payload: error
})

const setUserData = data => ({
  type: MatrixActionTypes.SET_USER_DATA,
  payload: data
})

const setRoomList = roomList => ({
  type: MatrixActionTypes.SET_ROOM_LIST,
  payload: roomList
})

export const setCurrentRoom = room => {
  // saveCurrentRoomToCache(room);
  return {
    type: MatrixActionTypes.SET_CURRENT_ROOM,
    payload: room
  }
}

const setCurMessages = payload => ({
  type: MatrixActionTypes.SET_CURRENT_MESSAGES,
  payload
})

export const updateIsSyncing = value => ({
  type: MatrixActionTypes.UPDATE_IS_SYNCING,
  payload: value
})

export const updateNewMessage = value => ({
  type: MatrixActionTypes.UPDATE_NEW_MSG,
  payload: value
})

const userLogout = () => ({
  type: MatrixActionTypes.USER_LOGOUT
})

//* *******************************************************************************
// Thunks
//* *******************************************************************************

export const createMatrixClient = (
  username,
  password,
  homeserver = 'https://matrix.org'
) => {
  debug('createClientThunk()')
  return async (dispatch, getState) => {
    const client = sdk.createClient(homeserver)
    dispatch(setMatrixClient(client))
    saveHomeserverToCache(homeserver)
    client
      .login('m.login.password', { user: username, password })
      .then(({ user_id: userId, access_token: accessToken }) => {
        const userData = {
          userId: userId,
          accessToken: accessToken,
          displayName: userId
        }
        debug('user data', userData)
        saveUserDataToCache(userData)
        dispatch(setUserData(userData))
        dispatch(startMatrixClient())
      })
      .catch(err => {
        debug('Matrix Auth Error: ', err)
        dispatch(setAuthError(err))
      })
  }
}

const refreshClientFromToken = () => {
  return async (dispatch, getState) => {
    logCache()
    const { accessToken, userId } = await retrieveUserDataFromCache()
    const homeserver = await retrieveHomeserverFromCache()
    const client = sdk.createClient({
      baseUrl: homeserver,
      accessToken,
      userId
    })
    dispatch(setMatrixClient(client))
    dispatch(startMatrixClient())
  }
}

export const reloadFromCache = () => {
  debug('reloadFromCache()')
  return async (dispatch, getState) => {
    const userDataPromise = retrieveUserDataFromCache()
    // const roomListPromise = retrieveRoomListFromCache();
    // const currentRoomPromise = retrieveCurrentRoomFromCache();

    dispatch(setUserData(await userDataPromise))
    // dispatch(setRoomList(await roomListPromise));
    // dispatch(setCurrentRoom(await currentRoomPromise));
  }
}

export const startMatrixClient = () => {
  debug('startMatrixClient()')
  return async (dispatch, getState) => {
    const client = clientSelector(getState())
    if (!client) {
      dispatch(refreshClientFromToken())
      return
    }
    debug('we have a client')

    client.on('sync', (state, prevState, data) =>
      onSync(state, prevState, data, dispatch, getState, client)
    )

    client.on('Room.timeline', (data, room) =>
      onTimeline(data, room, dispatch, getState, client)
    )

    client.on('RoomMember.typing', (event, member) => {
      const typingUser = {
        userId: member.userId,
        roomId: member.roomId,
        typing: member.typing,
        displayName: member.name
      }
      dispatch(updateTypingUsers(typingUser))
    })

    const opts = {
      initialSyncLimit: 8,
      lazyLoadMembers: true
    }
    client.startClient(opts)
  }
}

const onSync = async (state, prevState, data, dispatch, getState, client) => {
  try {
    debug('SYNCING STATE', state)
    const roomList = client.getVisibleRooms()
    const preparedRooms = prepareRooms(roomList, client)
    const curRoomList = roomListSelector(getState())
    switch (state) {
      case 'PREPARED':
        debug('room list', roomList)
        dispatch(setRoomList(preparedRooms))
        break
      case 'SYNCING':
        debug('STATE: ', data)
        dispatch(updateIsSyncing(false))
        if (!_.isEqual(roomList, curRoomList)) {
          dispatch(setRoomList(preparedRooms))
        }
        break
      case 'ERROR':
        debug('Sync Error: ', data.error)
        dispatch(setSyncError(data.error))
        break
    }
  } catch (error) {
    console.warn('error syncing', error)
  }
}

export const logoutUser = () => {
  return (dispatch, getState) => {
    const client = clientSelector(getState())
    client.stopClient()
    dispatch(setMatrixClient(null))
    dispatch(setRoomList([]))
    dispatch(setUserData(null))
    dispatch(userLogout())
    clearCache()
  }
}

const onTimeline = (data, room, dispatch, getState, client) => {
  const currentRoom = currentRoomSelector(getState())
  if (room.roomId !== currentRoom) return
  const isUser = userDataSelector(getState()).userId === data.getSender()
  if (!isUser) {
    const messages = [
      formatMatrixEvent(data),
      ...currentMessagesSelector(getState())
    ]
    const newMessages = _.uniqWith(
      messages,
      (a, b) => a.event_id === b.event_id
    )
    dispatch(setCurrentMessages(newMessages))
  }
}

export const setCurrentMessages = messages => {
  debug('setCurrentMessages()')
  return async dispatch => {
    const messageList = removeDuplicateEvents(messages)
    dispatch(setCurMessages(messageList))
  }
}

const prepareRooms = (rooms, client) => {
  rooms.sort((a, b) => {
    const timelineA = a.getLiveTimeline().getEvents()
    const timelineB = b.getLiveTimeline().getEvents()

    const lastEventA = timelineA[timelineA.length - 1]
    const lastEventB = timelineB[timelineB.length - 1]

    const timestampA = lastEventA.getTs()
    const timestampB = lastEventB.getTs()

    return timestampB - timestampA
  })

  // Get DMs
  const directEvent = client.getAccountData('m.direct')
  const directRooms = directEvent ? Object.keys(directEvent.getContent()) : []

  for (const room of rooms) {
    // See if room is direct
    const joinedMembers = room.getJoinedMemberCount()
    if (directRooms.includes(room.roomId) || joinedMembers <= 2) {
      room.isDirect = true
    } else {
      room.isDirect = false
    }

    // Get room avatar
    const avatarEvent = room
      .getLiveTimeline()
      .getState(sdk.EventTimeline.FORWARDS)
      .getStateEvents('m.room.avatar', '')
    const avatarUrl = avatarEvent ? avatarEvent.getContent().url : null
    let avatarSmall = avatarUrl
      ? room.getAvatarUrl(client.getHomeserverUrl(), 50, 50, 'crop')
      : ''
    if (avatarSmall === '' && room.isDirect) {
      const user = room.getAvatarFallbackMember()
      const userAvatar = user ? user.getMxcAvatarUrl() : null
      avatarSmall = userAvatar
        ? user.getAvatarUrl(client.getHomeserverUrl(), 50, 50, 'crop')
        : ''
    }
    room.avatarUrl = { small: avatarSmall }
  }

  return rooms
}

export const formatMatrixEvent = event => {
  const ret = {}
  try {
    ret.age = event.getAge()
    ret.content = event.getContent()
    ret.event_id = event.getId()
    ret.origin_server_ts = event.event.origin_server_ts
    ret.room_id = event.getRoomId()
    ret.sender = event.getSender()
    ret.type = event.getType()
    return ret
  } catch (error) {
    // debug('Could not format event: ', event);
    return event
  }
}

export const removeDuplicateEvents = events => {
  const ret = _.uniqWith(events, (a, b) => a.event_id === b.event_id)
  return ret
}

export const updateTypingUsers = user => {
  return (dispatch, getState) => {
    const typingUsers = typingUsersSelector(getState())
    if (user.typing) {
      dispatch({
        type: MatrixActionTypes.UPDATE_TYPING_USERS,
        payload: [...typingUsers, user]
      })
    } else {
      const newTypingUsers = typingUsers.filter(u => u.userId !== user.userId)
      dispatch({
        type: MatrixActionTypes.UPDATE_TYPING_USERS,
        payload: newTypingUsers
      })
    }
  }
}
