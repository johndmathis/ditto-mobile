import 'react-native-gesture-handler'

import React from 'react'
import { isIphoneX } from 'react-native-iphone-x-helper'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createMaterialTopTabNavigator } from 'react-navigation-tabs'

import LandingScreen from '../../scenes/auth/LandingScreen'
import LoadingScreen from '../../scenes/auth/LoadingScreen'
import LoginScreen from '../../scenes/auth/LoginScreen'
import SignUpOneScreen from '../../scenes/auth/SignUpOneScreen'
import SignUpTwoScreen from '../../scenes/auth/SignUpTwoScreen'
import ChatScreen from '../../scenes/chat/ChatScreen'
import HeaderRight from '../../scenes/chatList/components/HeaderRight'
import UserHeader from '../../scenes/chatList/components/UserHeader'
import DirectChatScreen from '../../scenes/chatList/DirectChatScreen'
import GroupChatScreen from '../../scenes/chatList/GroupChatScreen'
import NewChatDetailsScreen from '../../scenes/newChat/NewChatDetailsScreen'
import NewChatScreen from '../../scenes/newChat/NewChatScreen'
import SettingsScreen from '../../scenes/settings/SettingsScreen'
import theme from '../../theme'

const HomeSwipeStack = createMaterialTopTabNavigator(
  {
    DirectChats: {
      screen: DirectChatScreen,
      navigationOptions: {
        tabBarLabel: 'Messages'
      }
    },
    GroupChats: {
      screen: GroupChatScreen,
      navigationOptions: {
        tabBarLabel: 'Groups'
      }
    }
  },
  {
    initialRouteName: 'DirectChats',
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: theme.primaryTextColor,
      inactiveTintColor: theme.secondaryTextColor,
      style: {
        backgroundColor: theme.chromeColor,
        borderTopWidth: 0.3,
        borderTopColor: '#222',
        height: isIphoneX() ? 75 : undefined,
        paddingLeft: 40,
        paddingRight: 40
      },
      indicatorStyle: {
        height: 0
      },
      tabStyle: {
        alignSelf: 'center',
        padding: 0
      },
      labelStyle: {
        fontWeight: '700',
        fontSize: 18
      },
      upperCaseLabel: false
    }
  }
)

const appHeaderStyle = {
  backgroundColor: theme.chromeColor,
  borderBottomWidth: 0.3,
  borderBottomColor: '#222',
  height: 50
}

const appHeaderTitleStyle = {
  color: theme.primaryTextColor,
  fontSize: 16,
  fontWeight: 'bold'
}

const AppStack = createStackNavigator(
  {
    Home: HomeSwipeStack,
    Chat: ChatScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: appHeaderStyle,
      headerLeft: <UserHeader />,
      headerRight: <HeaderRight />
    }
  }
)

const SettingsScreenWithHeader = createStackNavigator(
  { SettingsScreen },
  {
    defaultNavigationOptions: {
      headerTitle: 'Settings',
      headerStyle: appHeaderStyle,
      headerTitleStyle: appHeaderTitleStyle
    }
  }
)

const CreateNewChatModalWithHeader = createStackNavigator(
  {
    NewChatScreen,
    NewChatDetails: NewChatDetailsScreen
  },
  {
    defaultNavigationOptions: {
      headerStyle: appHeaderStyle,
      headerTitleStyle: appHeaderTitleStyle
    }
  }
)

const AppStackWithModals = createStackNavigator(
  {
    Main: AppStack,
    Settings: SettingsScreenWithHeader,
    NewChat: CreateNewChatModalWithHeader
  },
  {
    initialRouteName: 'Main',
    mode: 'modal',
    headerMode: 'none'
  }
)

const AuthStack = createStackNavigator(
  {
    Landing: LandingScreen,
    SignUpOne: SignUpOneScreen,
    SignUpTwo: SignUpTwoScreen,
    Login: LoginScreen
  },
  {
    initialRouteName: 'Landing',
    headerMode: 'none'
  }
)

const AppNavigator = createSwitchNavigator(
  {
    Loading: LoadingScreen,
    Auth: AuthStack,
    App: AppStackWithModals
  },
  {
    initialRouteName: 'Loading',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false
    }
  }
)

export default createAppContainer(AppNavigator)
