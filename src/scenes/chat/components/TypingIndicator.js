import LottieView from 'lottie-react-native'
import React from 'react'
import styled from 'styled-components/native'

const debug = require('debug')('ditto:scenes:chat:components:TypingIndicator')

// type IProps = {
//   typingUsers: any[],
//   lastMessage: any,
//   roomId: String
// };

export default function TypingIndicator ({ typingUsers, lastMessage, roomId }) {
  if (typingUsers.length === 0) return null
  let currentRoomTyping = false
  typingUsers.forEach(user => {
    debug('testing', roomId)
    if (user.roomId === roomId) {
      currentRoomTyping = true
      debug('cur user', user)
    }
  })

  if (currentRoomTyping) {
    return (
      <TypingBubble>
        <LottieView
          source={require('../../../assets/animations/typing.json')}
          autoPlay
          loop
          style={{ width: 40, marginBottom: -4, marginTop: -1 }}
        />
      </TypingBubble>
    )
  } else return null
}

const TypingBubble = styled.View`
  background-color: ${({ theme }) => theme.chat.otherBubbleBackground};
  border-radius: 18;

  width: 56;

  padding-left: 8;
  padding-right: 8;

  margin-top: 2;
  margin-bottom: 4;
  margin-left: 8;
  margin-right: 8;

`
