import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { clientSelector } from '../../../selectors'
import ImageMessageBubble from './MessageTypes/ImageMessageBubble'
import NoticeBubble from './MessageTypes/NoticeBubble'
import TextMessageBubble from './MessageTypes/TextMessageBubble'

const debug = require('debug')('ditto:component:MessageBubble')

/**
 * TODO:
 * m.notice
 * m.video
 * m.relates_to (reactions)
 */

class TimelineRenderer extends Component {
  renderEvent = () => {
    const { message, displayNames } = this.props
    switch (message.type) {
      case 'm.room.message':
        return this.renderMessage()
      case 'm.room.member':
        return this.renderMembership()
      case 'm.room.avatar':
        return (
          <SystemMessage>
            {displayNames[message.sender] || message.sender} changed the room
            avatar.
          </SystemMessage>
        )
      case 'm.room.create':
        return (
          <SystemMessage style={{ marginBottom: 36 }}>
            {displayNames[message.sender] || message.sender} created the room.
            Hooray!
          </SystemMessage>
        )
      case 'm.room.name':
        return (
          <SystemMessage>
            {displayNames[message.sender] || message.sender} changed the chat
            name to "{message.content.name}"
          </SystemMessage>
        )
      case 'm.room.power_levels':
      case 'm.room.history_visibility':
      case 'm.room.guest_access':
      case 'm.room.join_rules':
      case 'm.reaction':
        return null
      default:
        debug('message type', message)
        return (
          <SystemMessage>
            Ditto does not support the type "{message.type}" yet.
          </SystemMessage>
        )
    }
  };

  renderMessage = () => {
    switch (this.props.message.content.msgtype) {
      case 'm.text':
        return <TextMessageBubble {...this.props} />
      case 'm.image':
        return <ImageMessageBubble {...this.props} />
      case 'm.notice':
        return <NoticeBubble {...this.props} />
      default:
        debug('message content type', this.props.message)
        return (
          <SystemMessage>
            Ditto does not support the message type "
            {this.props.message.content.msgtype}" yet.
          </SystemMessage>
        )
    }
  };

  renderMembership = () => {
    const { message, displayNames } = this.props
    switch (message.content.membership) {
      case 'invite':
        return (
          <SystemMessage>
            {displayNames[message.sender] || message.sender} invited{' '}
            {message.content.displayname}.
          </SystemMessage>
        )
      case 'leave':
        return (
          <SystemMessage>{message.content.displayname} has left.</SystemMessage>
        )
      default:
        debug('message type', message.content.membership, message)
        return (
          <SystemMessage>
            Ditto does not support the membership type "
            {message.content.membership}" yet.
          </SystemMessage>
        )
    }
  };

  render () {
    return this.renderEvent()
  }
}

const mapStateToProps = state => ({
  client: clientSelector(state)
})
export default connect(mapStateToProps)(TimelineRenderer)

const SystemMessage = styled.Text`
  color: rgba(255, 255, 255, 0.3);
  font-size: 12;
  align-self: center;
  margin-top: 8;
  margin-bottom: 4;
`
