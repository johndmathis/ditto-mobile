import React, { Component } from 'react'
import Html from 'react-native-render-html'
import styled from 'styled-components/native'

import { COLORS } from '../../../../constants'

export default class NoticeBubble extends Component {
  render () {
    const { message } = this.props
    const displayText = message.content.formatted_body || message.content.body
    return (
      <Wrapper>
        <BubbleText style={{ opacity: 0.3 }}>{message.sender}</BubbleText>
        <Bubble>
          <Html html={displayText} {...htmlProps} />
        </Bubble>
      </Wrapper>
    )
  }
}

const baseFontStyle = {
  color: '#fff',
  fontSize: 13,
  letterSpacing: 0.3,
  fontWeight: '400'
}

const tagsStyles = {
  blockquote: {
    borderLeftColor: COLORS.red,
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginVertical: 10,
    opacity: 0.8
  },
  p: {}
}

const htmlProps = {
  baseFontStyle,
  tagsStyles
}

const Wrapper = styled.View`
  align-items: center;
  justify-content: center;
  margin-top: 12;
  margin-bottom: 12;
`

const Bubble = styled.View`
  background-color: ${({ theme }) => theme.chat.otherBubbleBackground};
  /* width: 200; */
  padding-top: 8;
  padding-bottom: 8;
  padding-left: 14;
  padding-right: 14;
  border-radius: 18;
  margin-top: 4;
  max-width: 300;
`

const BubbleText = styled.Text`
  color: ${({ theme }) => theme.primaryTextColor};
`
