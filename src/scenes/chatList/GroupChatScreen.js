import React from 'react'
import styled from 'styled-components/native'

import ChatList from './components/ChatList'

const GroupChatScreen = () => (
  <Wrapper>
    <ChatList groups />
  </Wrapper>
)
export default GroupChatScreen

const Wrapper = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
`
