import { isEqual } from 'lodash'
import React, { Component } from 'react'
import { FlatList, View } from 'react-native'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { isSyncingSelector, roomListSelector } from '../../../selectors'
import { getChatSnippet } from '../../chat/Messages'
import {
  Avatar,
  AvatarBackground,
  AvatarText,
  AvatarTextWrapper,
  ChatListItem,
  ChatTitle,
  Snippet,
  Title
} from './ChatListComponents'

class ChatList extends Component {
  state = {
    chatList: []
  };

  navigateToChat = room => {
    this.props.navigation.navigate('Chat', {
      roomId: room.roomId,
      roomTitle: room.name
    })
  };

  updateChatList = () => {
    const directChats = this.props.rooms.filter(room => room.isDirect)
    const groupChats = this.props.rooms.filter(room => !room.isDirect)
    const chatList = this.props.groups ? groupChats : directChats
    this.setState({ chatList })
  };

  componentDidMount = () => {
    this.updateChatList()
  };

  componentDidUpdate = prevProps => {
    if (!isEqual(prevProps.rooms, this.props.rooms)) {
      this.updateChatList()
    }
  };

  render () {
    return (
      <>
        {this.props.isSyncing && (
          <SyncingIndicator>
            <SyncingText>Syncing...</SyncingText>
          </SyncingIndicator>
        )}
        <FlatList
          data={this.state.chatList}
          renderItem={({ item: room }) => {
            // debug(
            //   room.name,
            //   this.state.avatarUrls[room.roomId] === undefined,
            //   this.state.avatarUrls[room.roomId]
            // );
            return (
              <ChatListItem
                key={room.roomId}
                onPress={() => this.navigateToChat(room)}
              >
                <View
                  style={{
                    position: 'relative',
                    width: 50,
                    height: 50
                  }}
                >
                  <Avatar source={{ uri: room.avatarUrl.small }} />
                  <AvatarTextWrapper>
                    <AvatarText>{room.name[0]}</AvatarText>
                  </AvatarTextWrapper>
                  <AvatarBackground roomName={room.name} />
                </View>

                {/* <ImageBackground
                  source={{uri: this.state.avatarUrls[room.roomId]}}
                  style={{width: '100%', height: '100%'}}>
                  <Text>Inside</Text>
                </ImageBackground> */}
                <ChatTitle>
                  <Title numberOfLines={1}>{room.name}</Title>
                  <Snippet numberOfLines={1}>{getChatSnippet(room)}</Snippet>
                </ChatTitle>
              </ChatListItem>
            )
          }}
          keyExtractor={item => item.roomId}
        />
      </>
    )
  }
}
const mapState = state => ({
  rooms: roomListSelector(state),
  isSyncing: isSyncingSelector(state)
})
const mapDispatch = {}
export default connect(mapState, mapDispatch)(withNavigation(ChatList))

const SyncingIndicator = styled.View`
  background-color: #4e22be;
  height: 20;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const SyncingText = styled.Text`
  color: ${({ theme }) => theme.primaryTextColor};
  font-style: italic;
  font-size: 12;
`
