import React from 'react'
import Icon from 'react-native-vector-icons/Entypo'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { COLORS } from '../../../constants'
import { newChatModalVisibleSelector } from '../../../selectors'
import { updateNewChatModalVisible } from '../../../services/app/actions'

class HeaderRight extends React.PureComponent {
  handlePress = () => {
    this.props.navigation.navigate('NewChat')
  };

  render () {
    return (
      <Wrapper onPress={this.handlePress}>
        <Plus name='plus' size={25} color={COLORS.gray.one} />
      </Wrapper>
    )
  }
}
const mapState = state => ({
  modalVisible: newChatModalVisibleSelector(state)
})
const mapDispatch = {
  updateNewChatModalVisible
}
export default connect(mapState, mapDispatch)(withNavigation(HeaderRight))

const Wrapper = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.primaryAccentColor};
  margin-right: 14;
  border-radius: 30;
  width: 30;
  height: 30;
  justify-content: center;
  align-items: center;
`

const Plus = styled(Icon)`
  width: 25;
  height: 25;
`
