import Color from 'color'
import styled from 'styled-components/native'

import { getNameColor } from '../../../utilities/Misc'

export const ChatListItem = styled.TouchableOpacity`
  padding-left: 14;
  padding-right: 14;
  padding-top: 10;
  padding-bottom: 10;
  flex-direction: row;
  align-items: center;
  margin-top: 0.4;
  margin-bottom: 0.4;
  background-color: ${({ theme }) => theme.backgroundColor};
`

export const Avatar = styled.Image`
  position: absolute;
  top: 0;
  left: 0;
  width: 50;
  height: 50;
  border-radius: 40;
  z-index: 3;
`

export const AvatarTextWrapper = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  height: 50;
  width: 50;
  justify-content: center;
  align-items: center;
  z-index: 2;
`

export const AvatarText = styled.Text`
  color: ${({ theme }) => theme.primaryTextColor};
  font-size: 24;
`

export const AvatarBackground = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  height: 50;
  width: 50;
  border-radius: 40;
  background-color: ${({ roomName }) =>
    Color(getNameColor(roomName))
      .darken(0.5)
      .hex()};
  z-index: 1;
`

export const ChatTitle = styled.View`
  width: 300;
  margin-left: 14;
`

export const Title = styled.Text`
  font-weight: bold;
  font-size: 16;
  margin-bottom: 4;
  color: ${({ theme }) => theme.primaryTextColor};
`

export const Snippet = styled.Text`
  color: ${({ theme }) => theme.secondaryTextColor};
`
