import React, { useEffect, useState } from 'react'
import {
  ActivityIndicator,
  Linking,
  Switch,
  TouchableOpacity,
  View
} from 'react-native'
import { getApplicationName, getVersion } from 'react-native-device-info'
import ImagePicker from 'react-native-image-picker'
import { withNavigation } from 'react-navigation'
import { connect, useStore } from 'react-redux'
import styled from 'styled-components/native'

import ExitIcon from '../../assets/icons/icon-exit.svg'
import { Input, Text } from '../../components'
import { COLORS } from '../../constants'
import {
  clientSelector,
  pushNotificationsSelector,
  userDataSelector
} from '../../selectors'
import { updatePushNotifications } from '../../services/app/actions'
import { logoutUser } from '../../services/matrix/actions'
import {
  shutdownNotifications,
  startupNotifications
} from '../../services/notifications/Notifications'
import theme from '../../theme'
import { isIos, toImageBuffer } from '../../utilities/Misc'

const debug = require('debug')('ditto:screen:SettingsScreen')

const avatarSize = 80

const SettingsScreen = ({
  userData,
  client,
  pushNotifications,
  logoutUser,
  navigation
}) => {
  const store = useStore()
  debug('App state: ', store.getState())
  const currentDisplayName = userData.displayName
  const currentAvatarUrl = userData.avatarUrl
  const [displayName, setDisplayName] = useState(currentDisplayName)
  const [avatarUrl, setAvatarUrl] = useState(currentAvatarUrl)
  const [notifsSwitch, setNotifsSwitch] = useState({
    value: pushNotifications.enable && pushNotifications.pushkey != null,
    waiting: false
  })

  useEffect(() => {
    (async () => {
      const avatarData = await client.getProfileInfo(
        userData.userId,
        'avatar_url'
      )
      const url = client.mxcUrlToHttp(
        avatarData.avatar_url,
        avatarSize,
        avatarSize,
        'crop'
      )
      setAvatarUrl(url)
    })()
  })

  useEffect(() => {
    (async function () {
      setNotifsSwitch({
        value: pushNotifications.enable && pushNotifications.pushkey != null,
        waiting: false
      })
    })()
  }, [pushNotifications])

  const chooseAvatar = () => {
    debug('chooseAvatar')

    const options = { title: 'Choose avatar', mediaType: 'photo' }
    try {
      ImagePicker.showImagePicker(options, async response => {
        debug('client.uploadContent')
        const mxc = await client.uploadContent(toImageBuffer(response.data), {
          onlyContentUri: true,
          name: response.fileName,
          type: response.type
        })
        debug('client.uploadContent response', mxc)

        await client.setAvatarUrl(mxc)
        const url = client.mxcUrlToHttp(mxc, avatarSize, avatarSize, 'crop')
        debug('chooseAvatar:url ---', url)
        setAvatarUrl(url)
      })
    } catch (e) {
      debug('chooseAvatar error', e)
    }
  }

  const saveDisplayName = async () => {
    debug('save display name')
    if (currentDisplayName !== displayName) {
      try {
        await client.setDisplayName(displayName)
      } catch (e) {
        debug('saveDisplayName', e)
      }
    }
  }

  const switchNotifications = async enable => {
    // TODO: Disable switch upon waiting (+ icon?)
    if (isIos()) return
    if (enable) {
      setNotifsSwitch({
        value: true,
        waiting: true
      })
      debug('Switching on notifications')
      startupNotifications(true)
    } else {
      setNotifsSwitch({
        value: false,
        waiting: true
      })
      debug('Switching off notifications')
      shutdownNotifications(true)
    }
  }

  const logout = async () => {
    debug('logout')
    logoutUser()
    shutdownNotifications()
    navigation.navigate('Landing')
  }

  return (
    <SettingsWrapper>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <TouchableOpacity
          style={{ width: avatarSize, height: avatarSize }}
          onPress={chooseAvatar}
        >
          <Avatar source={{ uri: avatarUrl }} />
        </TouchableOpacity>
        <DisplayName
          onChangeText={setDisplayName}
          placeholder='Set display name'
          placeholderTextColor={COLORS.placeholderTextColor}
          value={displayName}
          onBlur={saveDisplayName}
        />
      </View>
      <Spacer />
      <InfoWrapper>
        <Text fontSize={18} style={{ marginRight: 12 }}>
          Enable notifications (Android)
        </Text>
        <Switch
          onValueChange={switchNotifications}
          value={notifsSwitch.value}
          disabled={notifsSwitch.waiting}
        />
        <ActivityIndicator animating={notifsSwitch.waiting} />
      </InfoWrapper>
      <Spacer />
      <InfoWrapper>
        <Text fontSize={18}>
          {`${getApplicationName()} version ${getVersion()}`}
        </Text>
      </InfoWrapper>
      <Spacer />
      <InfoWrapper>
        <InfoButton url='https://dittochat.org' text='Website' />
        <InfoLink> / </InfoLink>
        <InfoButton
          url='https://dittochat.org/privacy/'
          text='Privacy Policy'
        />
      </InfoWrapper>
      <InfoWrapper>
        <InfoButton url='https://plan.dittochat.org/' text='Request Feature' />
        <InfoLink> / </InfoLink>
        <InfoButton
          url='https://gitlab.com/ditto-chat/ditto-mobile/issues'
          text='Report Bug'
        />
      </InfoWrapper>
      <Spacer />
      <InfoWrapper>
        <TouchableOpacity onPress={logout}>
          <LogoutLink>Logout</LogoutLink>
        </TouchableOpacity>
      </InfoWrapper>
    </SettingsWrapper>
  )
}

SettingsScreen.navigationOptions = ({ navigation: nav }) => {
  return {
    headerRight: () => (
      <ExitButton onPress={() => nav.navigate('Main')}>
        <ExitIcon width={18} height={18} fill={theme.primaryTextColor} />
      </ExitButton>
    )
  }
}

const InfoButton = ({ url, text }) => {
  const openURL = url => async () => {
    try {
      debug('open website')
      await Linking.openURL(url)
    } catch (err) {
      debug('open website error', err)
    }
  }

  return (
    <TouchableOpacity onPress={openURL(url)}>
      <InfoLink>{text}</InfoLink>
    </TouchableOpacity>
  )
}

const ExitButton = styled.TouchableOpacity`
  width: 40;
  height: 40;
  margin-right: 6;
  justify-content: center;
  align-items: center;
`

const SettingsWrapper = styled.ScrollView`
  background-color: ${({ theme }) => theme.backgroundColor};
  height: 100%;
  padding-top: 30;
`

const Avatar = styled.Image`
  border-radius: 80;
  width: ${avatarSize};
  height: ${avatarSize};
  align-self: center;
`

const DisplayName = styled(Input)`
  width: 150;
  margin-left: 24;
  font-size: 18;
`

const InfoWrapper = styled.View`
  background-color: ${({ theme }) => theme.darkAccentColor};
  padding-top: 14;
  padding-bottom: 14;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const InfoLink = styled.Text`
  color: ${({ theme }) => theme.secondaryAccentColor};
  font-size: 18;
  text-align: center;
`

const LogoutLink = styled.Text`
  color: ${({ theme }) => theme.primaryAccentColor};
  text-align: center;
  font-size: 18;
`

const Spacer = styled.View`
  background-color: ${({ theme }) => theme.backgroundColor};
  height: 24;
`

const mapStateToProps = state => ({
  userData: userDataSelector(state),
  client: clientSelector(state),
  pushNotifications: pushNotificationsSelector(state)
})
const mapDispatchToProps = {
  logoutUser,
  updatePushNotifications
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(SettingsScreen))
