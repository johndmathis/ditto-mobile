import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/Entypo'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import ExitIcon from '../../assets/icons/icon-exit.svg'
import { Button, Input, Switch } from '../../components'
import { clientSelector } from '../../selectors'
import { setCurrentRoom } from '../../services/matrix/actions'
import { retrieveDisplayNameListFromCache } from '../../services/matrix/api'
import theme from '../../theme'

const debug = require('debug')('ditto:screen:NewChatDetailsScreen')

class NewChatDetailsScreen extends Component {
  static navigationOptions = ({ navigation: nav }) => {
    return {
      title: 'Message Details',
      headerLeft: <BackButton onPress={() => nav.goBack()} />,
      headerRight: () => (
        <ExitButton onPress={() => nav.navigate('Main')}>
          <ExitIcon width={18} height={18} fill={theme.primaryTextColor} />
        </ExitButton>
      )
    }
  };

  state = {
    visibility: 'private',
    name: 'My New Room',
    isLoading: false
  };

  handleToggle = visibility => {
    this.setState({ visibility: visibility ? 'private' : 'public' })
  };

  handleCreateChat = () => {
    const usersToInvite = this.props.navigation.getParam('usersToInvite')

    const options = {
      // room_alias_name: 'alias localpart to assign to the room',
      visibility: this.state.visibility,
      invite: usersToInvite,
      // name: this.state.name,
      room_topic: ''
    }
    debug('Room options: ', options)
    this.setState({ isLoading: true })
    this.props.client
      .createRoom(options, () => {
        this.setState({ isLoading: false })
      })
      .then(response => {
        const room = this.props.client.getRoom(response.room_id)
        this.props.setCurrentRoom(room.roomId)
        this.props.navigation.navigate('Chat', { roomId: room.roomId })
      })
  };

  componentDidMount = () => {
    const usersToInvite = this.props.navigation.getParam('usersToInvite')
    if (usersToInvite.length === 1) {
      const displayNames = retrieveDisplayNameListFromCache()
      this.setState({ name: displayNames[usersToInvite[0]] || usersToInvite[0] })
    }
  };

  render () {
    return (
      <Wrapper>
        <Label>Chat Name</Label>
        <Input
          value={this.state.name}
          onChangeText={name => this.setState({ name })}
          placeholder='Ex. My Awesome Room'
          placeholderTextColor={theme.placeholderTextColor}
          selectTextOnFocus
        />
        <Label>Privacy Setting</Label>
        <Switch
          leftLabel='Private'
          rightLabel='Public'
          onToggle={this.handleToggle}
        />
        <Button
          title='Create Chat'
          style={{ marginTop: 48 }}
          disabled={this.state.name.length === 0}
          onPress={this.handleCreateChat}
          isLoading={this.state.isLoading}
        />
      </Wrapper>
    )
  }
}
const mapState = state => ({
  client: clientSelector(state)
})
const mapDispatch = {
  setCurrentRoom
}
export default connect(mapState, mapDispatch)(NewChatDetailsScreen)

const BackTouchable = styled.TouchableOpacity``
const BackButton = ({ onPress }) => (
  <BackTouchable onPress={onPress}>
    <Icon
      name='chevron-left'
      size={35}
      color={theme.iconColor}
      style={{ marginLeft: 4 }}
    />
  </BackTouchable>
)

const ExitButton = styled.TouchableOpacity`
  width: 40;
  height: 40;
  margin-right: 6;
  justify-content: center;
  align-items: center;
`

const Wrapper = styled.ScrollView`
  padding-left: 12;
  padding-right: 12;
  background-color: ${({ theme }) => theme.backgroundColor};
`

const Label = styled.Text`
  color: ${({ theme }) => theme.secondaryTextColor};
  font-size: 20;
  margin-top: 24;
  margin-bottom: 12;
`
