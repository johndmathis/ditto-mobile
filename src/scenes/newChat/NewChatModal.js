import _ from 'lodash'
import React, { Component } from 'react'
import { FlatList, LayoutAnimation, Modal, Platform, View } from 'react-native'
import Image from 'react-native-scalable-image'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { BackIcon, Button } from '../../components'
import { COLORS, SCREEN_WIDTH } from '../../constants'
import { clientSelector, newChatModalVisibleSelector } from '../../selectors'
import { updateNewChatModalVisible } from '../../services/app/actions'
import { searchUserDirectory } from '../../services/matrix/api'

const debug = require('debug')('ditto:screen:NewChatModal')

const Placeholder = require('../../assets/images/placeholder.png')

class NewChatModal extends Component {
  state = {
    currentPage: 0,
    messageType: '',
    searchValue: '',
    searchResults: []
  };

  handleRequestModalClose = () => {
    this.props.updateNewChatModalVisible(false)
  };

  setCurrentPage = currentPage => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ currentPage })
  };

  handleBackPress = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ messageType: '', currentPage: 0 })
  };

  handleDirectMessagePress = () => {
    this.setState({ messageType: 'Direct Message' }, () =>
      this.setCurrentPage(1)
    )
  };

  handleGroupMessagePress = () => {
    this.setState({ messageType: 'Group Message' }, () => this.setCurrentPage(1))
  };

  handleChangeSearchTerm = async searchValue => {
    this.setState({ searchValue })
    const searchResults = await searchUserDirectory(searchValue)
    this.setState({ searchResults })
  };

  handleChangeTextDelayed = _.debounce(this.handleChangeSearchTerm, 300);

  renderSearchResults = ({ item }) => {
    const avatarUrl = this.props.client.mxcUrlToHttp(
      item.avatar_url,
      80,
      80,
      'crop'
    )
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Avatar source={{ uri: avatarUrl }} defaultSource={Placeholder} />
          <View style={{ marginLeft: 15 }}>
            <DisplayName>{item.display_name}</DisplayName>
            <UserId>{item.user_id}</UserId>
          </View>
        </View>
      </View>
    )
  };

  componentDidUpdate = prevProps => {
    if (prevProps.visible && !this.props.visible) {
      this.backToPageOne()
    }
  };

  render () {
    const { currentPage, messageType, searchResults } = this.state
    const pageZeroMargin = currentPage === 0 ? 0 : -SCREEN_WIDTH
    const pageOneMargin = currentPage === 1 ? 0 : SCREEN_WIDTH
    return this.props.visible ? (
      <Modal
        animationType='slide'
        presentationStyle='pageSheet'
        visible={this.props.visible}
        onRequestClose={this.handleRequestModalClose}
      >
        <ModalContent>
          {Platform.OS === 'ios' && <Handle />}
          <Page style={{ marginLeft: pageZeroMargin }}>
            <Title>Start a...</Title>
            <Button
              title='Direct Message'
              style={{ marginHorizontal: 15, marginBottom: 15 }}
              onPress={this.handleDirectMessagePress}
            />
            <Button
              title='Group Message'
              style={{ marginHorizontal: 15 }}
              onPress={this.handleGroupMessagePress}
            />
          </Page>
          <Page style={{ marginLeft: pageOneMargin }}>
            <TitleWrapper>
              <BackButton onPress={this.handleBackPress}>
                <BackIcon />
              </BackButton>
              <Title>{messageType}</Title>
              <View style={{ width: 30, marginRight: 15 }} />
            </TitleWrapper>
            <SearchBox onChangeText={this.handleChangeTextDelayed} />
            <FlatList
              data={searchResults}
              renderItem={this.renderSearchResults}
            />
          </Page>
        </ModalContent>
      </Modal>
    ) : null
  }
}
const mapState = state => {
  debug('App State', state)
  return {
    client: clientSelector(state),
    visible: newChatModalVisibleSelector(state)
  }
}
const mapDispatch = {
  updateNewChatModalVisible
}
export default connect(mapState, mapDispatch)(NewChatModal)

const ModalContent = styled.View`
  background-color: ${COLORS.dark};
  flex: 1;
  padding-top: 30;
  flex-direction: row;
`

const Handle = styled.View`
  height: 5;
  width: 50;
  background-color: ${COLORS.headerColor};
  position: absolute;
  top: 10;
  left: ${SCREEN_WIDTH * 0.43};
  border-radius: 20;
`

const Title = styled.Text`
  color: white;
  font-size: 26;
  font-weight: 600;
  text-align: center;
  margin-bottom: 20;
`

const TitleWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

const BackButton = styled.TouchableOpacity`
  width: 30;
  height: 30;
  margin-left: 15;
`

const Page = styled.View`
  width: ${SCREEN_WIDTH};
`

const SearchBox = styled.TextInput`
  background-color: blue;
  margin-left: 15;
  margin-right: 15;
  padding-left: 15;
  padding-right: 15;
  padding-top: 8;
  padding-bottom: 8;
  border-radius: 20;
  font-size: 22;
`

const Avatar = styled(Image)`
  width: 80;
  height: 80;
  border-radius: 75;
`

const DisplayName = styled.Text`
  font-size: 20;
  color: ${COLORS.gray.one};
`
const UserId = styled.Text``
