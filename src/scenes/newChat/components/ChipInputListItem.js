import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import styled from 'styled-components/native'

const Placeholder = require('../../../assets/images/placeholder.png')

export default class ChipInputListItem extends React.PureComponent {
  render () {
    const { id, onPress, avatarUrl, item } = this.props
    return (
      <TouchableOpacity
        key={id}
        onPress={onPress}
        style={{
          // flexDirection: 'row',
          // justifyContent: 'space-between',
          marginTop: 15
          // backgroundColor: 'yellow',
          // height: 60
          // width: 100
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Avatar source={{ uri: avatarUrl }} defaultSource={Placeholder} />
          <View style={{ marginLeft: 15 }}>
            <DisplayName>{item.display_name}</DisplayName>
            {item.user_id && <UserId>{item.user_id}</UserId>}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const Avatar = styled.Image`
  width: 60;
  height: 60;
  border-radius: 75;
`

const DisplayName = styled.Text`
  font-size: 20;
  color: ${({ theme }) => theme.primaryTextColor};
`
const UserId = styled.Text`
  color: ${({ theme }) => theme.secondaryTextColor};
`
