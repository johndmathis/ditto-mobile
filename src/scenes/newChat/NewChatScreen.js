import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import styled, { withTheme } from 'styled-components/native'

import ChevronsUp from '../../assets/icons/icon-chevrons-up.svg'
import ExitIcon from '../../assets/icons/icon-exit.svg'
import { userDataSelector } from '../../selectors'
import { searchUserDirectory } from '../../services/matrix/api'
import theme from '../../theme'
import ChipInput from './components/ChipInput'

const debug = require('debug')('ditto:screen:NewChatScreen')

class NewChatScreen extends Component {
  static navigationOptions = ({ navigation: nav }) => {
    return {
      title: 'New Message',
      headerRight: () => (
        <ExitButton onPress={() => nav.goBack(null)}>
          <ExitIcon width={18} height={18} fill={theme.primaryTextColor} />
        </ExitButton>
      )
    }
  };

  state = {
    searchValue: '',
    searchResults: [],
    isEnabled: false,
    selectedUsers: []
  };

  checkIsEnabled = isEnabled => {
    this.setState({ isEnabled })
  };

  handleChangeSearchTerm = async searchValue => {
    this.setState({ searchValue })
    const searchResults = await searchUserDirectory(searchValue)
    const cleanSearchResults = []
    for (const result of searchResults) {
      if (
        result.user_id !== this.props.currentUser.userId &&
        cleanSearchResults.findIndex(
          searchResult => searchResult.user_id === result.user_id
        ) === -1
      ) {
        cleanSearchResults.push(result)
      }
    }
    this.setState({
      searchResults: cleanSearchResults
    })
  };

  updateSelectedUsers = selectedUsers => this.setState({ selectedUsers });

  handleConfirmMembers = () => {
    debug('selected members', this.state.selectedUsers)
    this.props.navigation.navigate('NewChatDetails', {
      usersToInvite: this.state.selectedUsers
    })
  };

  render () {
    const { searchResults } = this.state
    debug('SEARCH RESULTS', searchResults)
    return (
      <Wrapper>
        <ModalContent>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center'
            }}
          >
            <ChipInput
              updateSelectedUsers={this.updateSelectedUsers}
              onChangeText={this.handleChangeSearchTerm}
              itemId='user_id'
              items={searchResults}
              onSubmitEditing={isEnabled => this.checkIsEnabled(isEnabled)}
              onChipClose={isEnabled => this.checkIsEnabled(isEnabled)}
            />
            <ActionButton
              style={{ padding: 10, alignSelf: 'flex-start' }}
              onPress={this.handleConfirmMembers}
              disabled={this.state.selectedUsers.length === 0}
            >
              <ChevronsUp
                fill={this.props.theme.iconColor}
                width={16}
                height={16}
                style={{ transform: [{ rotate: '90deg' }], marginLeft: 1 }}
              />
            </ActionButton>
          </View>
        </ModalContent>
      </Wrapper>
    )
  }
}
const mapState = state => ({
  currentUser: userDataSelector(state)
})
export default connect(mapState)(withTheme(NewChatScreen))

const Wrapper = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
`

const ModalContent = styled.View`
  background-color: ${({ theme }) => theme.backgroundColor};
  margin-top: 12;
`

const ActionButton = styled.TouchableOpacity`
  background-color: ${({ theme, disabled }) =>
    `${theme.primaryAccentColor}${disabled ? '85' : ''}`};
  justify-content: center;
  align-items: center;
  border-radius: 40;
  margin-left: 8;
  margin-right: 8;
  width: 35;
  height: 35;
  align-self: flex-end;
`

const ExitButton = styled.TouchableOpacity`
  width: 40;
  height: 40;
  margin-right: 6;
  justify-content: center;
  align-items: center;
`
