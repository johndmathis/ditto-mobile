import Color from 'color'
import React, { Component } from 'react'
import Image from 'react-native-scalable-image'
import styled from 'styled-components/native'

// import DittoIcon from '../../assets/icons/DittoIcon';
import BackIcon from '../../assets/icons/icon-chevron-left.svg'
import { Button, Input, PageWrapper, Text } from '../../components'
import { COLORS, SCREEN_WIDTH } from '../../constants'
import AuthFooter from './components/AuthFooter'

const topBlob = require('../../assets/images/blob4.png')
const bottomBlob = require('../../assets/images/blob5.png')

export default class SignUpOneScreen extends Component {
  handleBackPress = () => this.props.navigation.navigate('Landing');
  navToLogin = () => this.props.navigation.navigate('Login');
  handleNextPress = () => this.props.navigation.navigate('SignUpTwo');

  render () {
    const placeholderTextColor = Color(COLORS.gray.one)
      .alpha(0.2)
      .rgb()
      .string()
    return (
      <PageWrapper>
        <TopBlob source={topBlob} />
        <BottomBlob source={bottomBlob} />
        <BackButton onPress={this.handleBackPress}>
          <BackIcon name='chevron-left' size={30} />
        </BackButton>
        <PageMargin>
          <Title>Let's get started</Title>
          <SignUpInput
            placeholder='First Name'
            placeholderTextColor={placeholderTextColor}
          />
          <SignUpInput
            placeholder='Last Name'
            placeholderTextColor={placeholderTextColor}
          />
          <SignUpInput
            placeholder='Email'
            placeholderTextColor={placeholderTextColor}
          />
          <ErrorText>Looks like that email is invalid. Try again!</ErrorText>
        </PageMargin>
        <ButtonWrapper>
          <Button title='Next' onPress={this.handleNextPress} />
        </ButtonWrapper>
        <AuthFooter mainText='Login' mainAction={this.navToLogin} />
      </PageWrapper>
    )
  }
}

const PageMargin = styled.View`
  margin-left: 30;
  margin-right: 30;
`

const TopBlob = styled(Image)`
  position: absolute;
  top: -285;
  left: -100;
`

const BottomBlob = styled(Image)`
  position: absolute;
  bottom: -500;
  left: -400;
`

const BackButton = styled.TouchableOpacity`
  padding-top: 20;
  padding-right: 30;
  padding-bottom: 30;
  padding-left: 30;
`

// const BackIcon = styled(DittoIcon)`
//   color: ${({theme}) => theme.dittoWhite};
// `;

const Title = styled(Text)`
  font-weight: bold;
  font-size: 30;
  margin-top: 30;
  margin-bottom: 45;
`

const SignUpInput = styled(Input)`
  margin-bottom: 15;
`

const ErrorText = styled(Text)`
  color: ${COLORS.red};
  font-size: 16;
  width: 225;
  text-align: center;
  align-self: center;
`

const ButtonWrapper = styled.View`
  position: absolute;
  bottom: 140;
  width: ${SCREEN_WIDTH};
  justify-content: center;
  align-items: center;
`
