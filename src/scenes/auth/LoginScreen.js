import Color from 'color'
import React, { useEffect, useRef, useState } from 'react'
import { View } from 'react-native'
import Image from 'react-native-scalable-image'
import { useNavigation } from 'react-navigation-hooks'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components/native'

import { BackIcon, Button, Input, PageWrapper, Text } from '../../components'
import { COLORS, SCREEN_HEIGHT, SCREEN_WIDTH } from '../../constants'
import { authErrorSelector, userDataSelector } from '../../selectors'
import {
  clearAuthError,
  createMatrixClient
} from '../../services/matrix/actions'
import { startupNotifications } from '../../services/notifications/Notifications'
// import DittoIcon from '../../assets/icons/DittoIcon';

const topBlob = require('../../assets/images/blob4.png')
const bottomBlob = require('../../assets/images/blob5.png')

export default function LoginScreen () {
  const { navigate } = useNavigation()
  const passwordInput = useRef(null)
  // State
  const [usernameValue, setUsernameValue] = useState('')
  const [passwordValue, setPasswordValue] = useState('')
  const [homeserverValue, setHomeserverValue] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  // Redux
  const dispatch = useDispatch()
  const authError = useSelector(authErrorSelector)
  const userData = useSelector(userDataSelector)

  useEffect(() => {
    if (authError != null) {
      setIsLoading(false)
    } else if (userData?.accessToken != null) {
      setIsLoading(false)
      startupNotifications()
      navigate('App')
    }
  }, [authError, isLoading, userData, navigate])

  const handleBackPress = () => navigate('Landing')
  // const navToSignUp = () => navigate('SignUpOne')

  const handleLoginPress = () => {
    dispatch(clearAuthError())
    setIsLoading(true)

    if (homeserverValue.length === 0) {
      dispatch(createMatrixClient(usernameValue, passwordValue))
    } else {
      dispatch(createMatrixClient(
        usernameValue,
        passwordValue,
        `https://${homeserverValue}`
      ))
    }
  }

  const placeholderTextColor = Color(COLORS.gray.one)
    .alpha(0.2)
    .rgb()
    .string()

  return (
    <PageWrapper>
      <TopBlob source={topBlob} />
      <BottomBlob source={bottomBlob} />
      <BackButton onPress={handleBackPress}>
        <BackIcon />
      </BackButton>
      <Title>Welcome back!</Title>

      <PageMargin keyboardShouldPersistTaps='handled'>
        <SignUpInput
          autoFocus
          value={usernameValue}
          onChangeText={text => setUsernameValue(text)}
          placeholder='Username'
          placeholderTextColor={placeholderTextColor}
          returnKeyType='next'
          onSubmitEditing={() => passwordInput.current.focus()}
          autoCapitalize='none'
        />
        <SignUpInput
          ref={passwordInput}
          secureTextEntry
          value={passwordValue}
          onChangeText={text => setPasswordValue(text)}
          placeholder='Password'
          placeholderTextColor={placeholderTextColor}
          autoCapitalize='none'
        />
        <InputWrapper>
          <HomeserverInput
            value={homeserverValue}
            onChangeText={text => setHomeserverValue(text)}
            placeholder='matrix.org (optional)'
            placeholderTextColor={placeholderTextColor}
            autoCapitalize='none'
            returnKeyType='go'
            onSubmitEditing={handleLoginPress}
          />
          <FakeHttpText>https://</FakeHttpText>
        </InputWrapper>
        {authError && (
          <ErrorText>
            Looks like something went wrong.{'\n'}Try again!
          </ErrorText>
        )}
        <ButtonWrapper>
          <Button
            title='Login'
            isLoading={isLoading}
            onPress={handleLoginPress}
          />
        </ButtonWrapper>
        <View style={{ height: SCREEN_HEIGHT / 2 }} />
      </PageMargin>

      {/* <AuthFooter mainText='Register' mainAction={navToSignUp} /> */}
    </PageWrapper>
  )
}

const PageMargin = styled.ScrollView`
  margin-left: 30;
  margin-right: 30;
  padding-top: 50;
`

const TopBlob = styled(Image)`
  position: absolute;
  top: -285;
  left: -100;
`

const BottomBlob = styled(Image)`
  position: absolute;
  bottom: -500;
  left: -400;
`

const BackButton = styled.TouchableOpacity`
  width: 60;
  height: 60;
  justify-content: center;
  align-items: center;
`

// const BackIcon = styled(DittoIcon)`
//   color: ${({theme}) => theme.dittoWhite};
// `;

const SignUpInput = styled(Input)`
  margin-bottom: 15;
`

const InputWrapper = styled.View`
  position: relative;
  flex-direction: row;
`

const HomeserverInput = styled(SignUpInput)`
  padding-left: 93;
  margin-bottom: 0;
  width: 100%;
`

const FakeHttpText = styled(Text)`
  position: absolute;
  align-self: center;
  margin-left: 15;
`

const ErrorText = styled(Text)`
  color: ${COLORS.red};
  font-size: 16;
  width: 300;
  text-align: center;
  align-self: center;
  margin-top: 20;
`

const ButtonWrapper = styled.View`
  width: ${SCREEN_WIDTH};
  justify-content: center;
  align-items: center;
  align-self: center;
  margin-top: 50;
`

const Title = styled(Text)`
  font-weight: bold;
  font-size: 30;
  margin-top: 30;
  margin-left: 30;
`
