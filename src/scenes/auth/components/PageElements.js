import React from 'react'
import Image from 'react-native-scalable-image'
import styled from 'styled-components/native'

import { responsive } from '../../../utilities/ResponsiveValue'
import Blob from './Blob'

const TopBlob = styled(Blob)`
  position: absolute;
  top: -200;
  left: -60;
  z-index: -1;
`

const BottomBlob = styled(Blob)`
  position: absolute;
  bottom: -225;
  left: -300;
  transform: rotate(-10deg);
  z-index: -1;
`

const Touchable = styled.TouchableOpacity`
  padding: ${responsive(10)}px;
  align-self: flex-start;
  margin: ${responsive(10)}px;
  z-index: 1;
`

const BackButton = ({ onPress }) => (
  <Touchable onPress={onPress}>
    <Image
      source={require('../../assets/images/chevron-left.png')}
      width={100}
    />
  </Touchable>
)

const Title = styled.Text`
  text-align: center;
  color: #fff;
  font-size: 35;
  font-weight: bold;
  margin: 10px 30px 8px;
`

const WordMark = styled(Image)`
  align-self: center;
  margin: 8px 30px 10px;
`

export { TopBlob, BottomBlob, BackButton, Title, WordMark }
