import React, { Component } from 'react'
import { StatusBar, View } from 'react-native'
import Image from 'react-native-scalable-image'
import styled from 'styled-components/native'

// import DittoIcon from '../../assets/icons/DittoIcon';
import WordmarkFile from '../../assets/icons/wordmark.svg'
import { Button, PageWrapper } from '../../components'
import { COLORS, SCREEN_WIDTH } from '../../constants'
// import AuthFooter from './components/AuthFooter'

const middleBlob = require('../../assets/images/blob1.png')
const topBlob = require('../../assets/images/blob2.png')
const bottomBlob = require('../../assets/images/blob3.png')

StatusBar.setBarStyle('light-content')

export default class LandingScreen extends Component {
  navToSignUp = () => this.props.navigation.navigate('SignUpOne');
  handleLoginPress = () => this.props.navigation.navigate('Login');

  render () {
    return (
      <PageWrapper>
        <TopBlob source={topBlob} />
        <MiddleBlob source={middleBlob} />
        <BottomBlob source={bottomBlob} />
        <Wordmark width={200} fill={COLORS.gray.one} />
        <View
          style={{
            flex: 1,
            marginBottom: 125,
            justifyContent: 'flex-end'
          }}
        >
          {/* <ButtonWrapper>
            <Button disabled title='Sign Up' onPress={this.navToSignUp} />
          </ButtonWrapper> */}
          <ButtonWrapper>
            <Button title='Login' onPress={this.handleLoginPress} />
          </ButtonWrapper>
        </View>
        {/* <AuthFooter mainText='Login' mainAction={this.navToLogin} /> */}
      </PageWrapper>
    )
  }
}

const TopBlob = styled(Image)`
  position: absolute;
  top: -80;
  right: -100;
`

const MiddleBlob = styled(Image)`
  position: absolute;
  top: -220;
  left: -420;
`

const BottomBlob = styled(Image)`
  position: absolute;
  bottom: -340;
  left: -220;
`

const Wordmark = styled(WordmarkFile)`
  margin-top: 100;
  margin-left: 30;
`

const ButtonWrapper = styled.View`
  /* position: absolute;
  bottom: 180; */
  justify-content: center;
  align-items: center;
  width: ${SCREEN_WIDTH};
  padding-top: 20;
  padding-bottom: 20;
`
