import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { COLORS } from '../../constants'
import { clientSelector, pushNotificationsSelector } from '../../selectors'
import {
  reloadFromCache,
  startMatrixClient
} from '../../services/matrix/actions'
import { retrieveUserDataFromCache } from '../../services/matrix/api'
import {
  getInitialNotification,
  startupNotifications
} from '../../services/notifications/Notifications'

const debug = require('debug')('ditto:screen:AuthLoadingScreen')

class AuthLoadingScreen extends Component {
  componentDidMount = async () => {
    const userData = await retrieveUserDataFromCache()
    debug('User data: ', userData)
    if (userData) {
      this.props.reloadFromCache()
      this.props.startMatrixClient()
      if (this.props.pushNotifications.enable) {
        startupNotifications()
      }
      const notification = await getInitialNotification()
      if (notification != null && notification.room != null && notification.room.id != null) {
        debug('Ditto was started by a notification tap: ', notification)
        this.props.navigation.navigate('Chat', { roomId: notification.room.id })
      } else {
        this.props.navigation.navigate('App')
      }
    } else {
      this.props.navigation.navigate('Auth')
    }
  };

  render () {
    return <Screen />
  }
}
const mapState = state => ({
  client: clientSelector(state),
  pushNotifications: pushNotificationsSelector(state)
})
const mapDispatchToProps = {
  startMatrixClient,
  reloadFromCache
}
export default connect(mapState, mapDispatchToProps)(AuthLoadingScreen)

const Screen = styled.View`
  flex: 1;
  background-color: ${COLORS.dark};
`
